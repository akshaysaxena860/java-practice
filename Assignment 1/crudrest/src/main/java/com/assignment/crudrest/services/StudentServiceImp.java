package com.assignment.crudrest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.crudrest.exception.DataNotFound;
import com.assignment.crudrest.model.Student;
import com.assignment.crudrest.repository.StudentRepository;

@Service
public class StudentServiceImp implements StudentService {

	@Autowired
	private StudentRepository studentRepository;
	
	@Override
	public List<Student> getallStudents() {
		return studentRepository.findAll();
	}

	@Override
	public Student addnewStudent(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public String deletestudent(Long id) throws DataNotFound {
Student student=studentRepository.findById(id).orElseThrow(() -> new DataNotFound("Failed To delete student"));
		
		studentRepository.delete(student);
		return "Student Deleted Successfully";

	} 

	@Override
	public String updatestudent(Student stu, Long id) throws DataNotFound {
Student student=studentRepository.findById(id).orElseThrow(() -> new DataNotFound("Failed To update student"));
		
		student.setFname(stu.getFname());
		student.setEmail(stu.getEmail());
		student.setLname(stu.getLname());
		student.setMarks(stu.getMarks());
		studentRepository.save(student);
		return "Student Data Updated Successfully";
	
	}

}
