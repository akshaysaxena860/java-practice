package com.assignment.crudrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.crudrest.exception.DataNotFound;
import com.assignment.crudrest.model.Student;

import com.assignment.crudrest.services.StudentService;

@RestController
public class StudentController {
	
	
@Autowired
private StudentService studentService;
	
//	Get list of all students
	@GetMapping("/students")
	public List<Student> getAllStudents()
	{
		return this.studentService.getallStudents();
	}
	
//	create New Student
	@PostMapping("/addstudent")
	public Student addNewStudent(@RequestBody Student student)
	{
		return this.studentService.addnewStudent(student);
	}
	
	
//	Update Student Info
	@PutMapping("/updatestudent/{id}")
	public String updateStudent(@RequestBody Student stu,@PathVariable(value = "id") Long id) throws DataNotFound
	{
		return this.studentService.updatestudent(stu, id);
	}
	
//	Delete Student Record
	@DeleteMapping("/deletestudent/{id}")
	public String deleteStudent(@PathVariable(value = "id") Long id) throws DataNotFound
	{
		return this.studentService.deletestudent(id);
	}
}
