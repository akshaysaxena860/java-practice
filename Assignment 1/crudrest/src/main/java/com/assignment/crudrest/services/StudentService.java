package com.assignment.crudrest.services;

import java.util.List;

import com.assignment.crudrest.exception.DataNotFound;
import com.assignment.crudrest.model.Student;

public interface StudentService {

	public List<Student> getallStudents();
	public Student addnewStudent(Student student);
	public String deletestudent(Long id) throws DataNotFound;
	public String updatestudent(Student stu,Long id) throws DataNotFound;
}
